// JavaScript source code
const path = require('path');
const root = path.resolve(__dirname, './FanfictionFinder/wwwroot');
const dist = path.resolve(root, 'dist');

module.exports = mode => {
    return [
        //{
        //    name: 'Site Utils',
        //    entry: path.resolve(root, './site.js'),
        //    output: {
        //        path: dist,
        //        filename: 'site.bundle.js',
        //    },
        //    mode: mode,
        //},
        // TODO: Decide on editor features.
        // https://ckeditor.com/ckeditor-5/online-builder/
        //{
        //    name: 'CK Editor Interop',
        //    entry: path.resolve(root, './ckeditor/interop/ckEditorInterop.js'),
        //    output: {
        //        path: dist,
        //        filename: 'ckEditorInterop.bundle.js',
        //    },
        //    mode: mode,
        //},
        {
            name: 'Split Interop',
            entry: path.resolve(root, './split/splitInterop.js'),
            output: {
                path: dist,
                filename: 'splitInterop.bundle.js'
            },
            mode: mode,
        },
        {
            name: 'Drop Interop',
            entry: path.resolve(root, './drag/dropInterop.js'),
            output: {
                path: dist,
                filename: 'dropInterop.bundle.js',
            },
            mode: mode,
        }
    ];
};

module.exports.parallelism = 2;