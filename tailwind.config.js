function withOpacityValue(variable) {
	return ({ opacityValue }) => {
		if (opacityValue === undefined) {
			return `rgb(var(${variable}))`
		}
		return `rgb(var(${variable}) / ${opacityValue})`
	}
}

module.exports = {
	mode: 'jit',
	content: [
		'../FanfictionFinder/**/*.{html,razor,razor.css,cshtml,cshtml.css}',
		'../FanfictionFinder.Components/**/*.{html,razor,razor.css,cshtml,cshtml.css}'
	],
	plugins: [
		require('@tailwindcss/forms'),
		require('@tailwindcss/typography'),
	],
}
