#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
ARG netversion=7.0

# do setup for the actual app.
FROM mcr.microsoft.com/dotnet/aspnet:$netversion AS base
WORKDIR /app

EXPOSE 80

# Install/restore dependencies.
FROM mcr.microsoft.com/dotnet/sdk:$netversion AS prebuild
ARG netversion

WORKDIR /src
COPY ["FanfictionFinder/FanfictionFinder.csproj", "FanfictionFinder/"]
COPY ["FanfictionFinder.Components/FanfictionFinder.Components.csproj", "FanfictionFinder.Components/"]
COPY ["FanfictionFinder.Data/FanfictionFinder.Data.csproj", "FanfictionFinder.Data/"]
RUN dotnet restore "FanfictionFinder/FanfictionFinder.csproj"
COPY . .

# Install and run webpack.
FROM node:18 as nodebuild
ARG netversion
COPY --from=prebuild /src /src

WORKDIR /src
RUN npm install
RUN npx webpack --mode=production --config webpack.config.js

# Build the project.
FROM prebuild as build
COPY --from=nodebuild /src /src

WORKDIR "/src/FanfictionFinder"
RUN dotnet build "FanfictionFinder.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/build

# Run postcss commands.
FROM nodebuild AS postbuild
COPY --from=build /src /src

WORKDIR "/src/FanfictionFinder"
RUN npx postcss obj/Release/net${netversion}/scopedcss/bundle/FanfictionFinder.styles.css -o wwwroot/css/scoped/project.min.css
RUN npx postcss obj/Release/net${netversion}/scopedcss/projectbundle/FanfictionFinder.bundle.scp.css -o wwwroot/css/scoped/bundle.min.css

WORKDIR "/src/FanfictionFinder.Components"
RUN npx postcss obj/Release/net${netversion}/scopedcss/bundle/FanfictionFinder.Components.styles.css -o wwwroot/css/scoped/project.min.css
RUN npx postcss obj/Release/net${netversion}/scopedcss/projectbundle/FanfictionFinder.Components.bundle.scp.css -o wwwroot/css/scoped/bundle.min.css

# publish the app.
FROM build AS publish
COPY --from=postbuild /src /src

WORKDIR "/src/FanfictionFinder"
RUN dotnet publish "FanfictionFinder.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/publish -p:UseAppHost=false

# execute the app.
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FanfictionFinder.dll"]
