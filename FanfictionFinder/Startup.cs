﻿using FanfictionFinder.Data.Database;
using FanfictionFinder.Data.Services;

using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace FanfictionFinder;

public class Startup
{
	public Startup(IConfiguration configuration)
	{
		Configuration = configuration;
	}

	public IConfiguration Configuration { get; }

	// This method gets called by the runtime. Use this method to add services to the container.
	// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
	public void ConfigureServices(IServiceCollection services)
	{
		#region Database Setup

		services.AddDbContextFactory<ApplicationDbContext>(options =>
			options.UseNpgsql(
#if DEBUG
				Configuration.GetConnectionString("development")
#else
                Configuration.GetConnectionString("release")
#endif
			)
#if DEBUG
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
#endif
			// Warnings for loading multiple collections without splitting the query.
			// We want this behaviour for accurate data loading (and our lists are not
			// of any large size for the roster) so we are ignoring these.
			.ConfigureWarnings(w =>
				w.Ignore(RelationalEventId.MultipleCollectionIncludeWarning)
			), ServiceLifetime.Singleton);

		services.AddScoped(p
			=> p.GetRequiredService<IDbContextFactory<ApplicationDbContext>>().CreateDbContext());

		#endregion
	}

	// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		// Configure the HTTP request pipeline.
		if (env.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
			app.UseMigrationsEndPoint();
		}
		else
		{
			app.UseExceptionHandler("/Error");
			// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
			app.UseHsts();
		}

		Task.Run(async () =>
		{
			using var scope = app.ApplicationServices.CreateScope();
			var _logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();

			_logger.LogInformation("Running Startup Tasks...");
			_logger.LogInformation("Validating Database...");

			#region Database Validation
			var dbFac = scope.ServiceProvider.GetRequiredService<IDbContextFactory<ApplicationDbContext>>();
			await using var db = await dbFac.CreateDbContextAsync();
			ApplyDatabaseMigrations(db);
			#endregion

			_logger.LogInformation("Validating Upload Locations...");

			#region File Validation
			var webHostEnvironment = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();
			var unsafeUploadsPath = Path.Combine(webHostEnvironment.ContentRootPath,
				webHostEnvironment.EnvironmentName, "unsafe_uploads");

			Directory.CreateDirectory(unsafeUploadsPath);
			#endregion

			_logger.LogInformation("Initalizing Hot Reload...");

			#region Hot Reload Setup
			// We just want to initalize it.
			_ = scope.ServiceProvider.GetRequiredService<HotReloadHandler>();
			#endregion

			_logger.LogInformation("... Startup Tasks Completed.");
		}).GetAwaiter().GetResult();

		app.UseForwardedHeaders(new ForwardedHeadersOptions()
		{
			ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
		});

		//app.UseHttpsRedirection();
		app.UseStaticFiles();

		app.UseRouting();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseEndpoints(endpoints =>
		{
			endpoints.MapControllers();
			endpoints.MapBlazorHub();
			endpoints.MapFallbackToPage("/_Host");
		});
	}

	private static void ApplyDatabaseMigrations(DbContext database)
	{
		try
		{
			if (!(database.Database.GetPendingMigrations()).Any())
			{
				return;
			}

			database.Database.Migrate();
			database.SaveChanges();
		}
		catch
		{
			Console.WriteLine("[ERROR] Failed to get/update migrations.");
		}
	}
}
