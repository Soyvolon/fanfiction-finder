﻿global using Microsoft.EntityFrameworkCore;

global using FanfictionFinder.Data.Structures;
global using FanfictionFinder.Data.Account;
global using FanfictionFinder.Data.Database;

global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Text;
global using System.Threading.Tasks;