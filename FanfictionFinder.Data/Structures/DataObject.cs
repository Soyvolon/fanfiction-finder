﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanfictionFinder.Data.Structures;

public abstract class DataObject
{
	public DateTime LastEdit { get; set; }
}

public abstract class DataObject<T> : DataObject
	where T : unmanaged
{
	public T Key { get; set; }
}
