﻿using FanfictionFinder.Data.Account;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanfictionFinder.Data.Database;
public class ApplicationDbContext : IdentityDbContext<FanfictionFinderUser, FanfictionFinderRole, Guid>
{
	public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
		: base(options)
	{
		SavingChanges += OnSave_UpdateTimestamps;
	}

	private void OnSave_UpdateTimestamps(object? sender, SavingChangesEventArgs e)
	{
		var now = DateTime.UtcNow;
		foreach (var entry in ChangeTracker.Entries())
			if (entry.Entity is DataObject data)
				data.LastEdit = now;
	}

	public override void Dispose()
	{
		SavingChanges -= OnSave_UpdateTimestamps;

		base.Dispose();
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);


	}
}
