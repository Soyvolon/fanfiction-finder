﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanfictionFinder.Data.Account;
public class FanfictionFinderRoleManager : RoleManager<FanfictionFinderRole>
{
	public FanfictionFinderRoleManager(IRoleStore<FanfictionFinderRole> store, 
		IEnumerable<IRoleValidator<FanfictionFinderRole>> roleValidators,
		ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, 
		ILogger<RoleManager<FanfictionFinderRole>> logger)
			: base(store, roleValidators, keyNormalizer, errors, logger)
	{

	}
}
