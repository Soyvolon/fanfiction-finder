﻿using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanfictionFinder.Data.Account;
public class FanfictionFinderRole : IdentityRole<Guid>
{
	public FanfictionFinderRole() : base() { }
	public FanfictionFinderRole(string name) : base(name) { }
}
